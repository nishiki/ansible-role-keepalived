# Ansible role: Keepalived

[![Version](https://img.shields.io/badge/latest_version-1.1.0-green.svg)](https://code.waks.be/nishiki/ansible-role-keepalived/releases)
[![License](https://img.shields.io/badge/license-Apache--2.0-blue.svg)](https://code.waks.be/nishiki/ansible-role-keepalived/src/branch/main/LICENSE)
[![Build](https://code.waks.be/nishiki/ansible-role-keepalived/actions/workflows/molecule.yml/badge.svg?branch=main)](https://code.waks.be/nishiki/ansible-role-keepalived/actions?workflow=molecule.yml)

Install and configure Keepalived

## Requirements

- Ansible >= 2.9
- Debian
  - Bullseye
  - Bookworm

## Role variables

| Name                          | Type  | Required | Default               | Comment                           |
| ----------------------------- | ----- | -------- | --------------------- | --------------------------------- |
| keepalived_mail_from          | str   | no       | keepalived@test.local | from header for notification mail |
| keepalived_smtp_server        | str   | no       | localhost             | the smtp server                   |
| keepalived_notification_mails | array | no       |                       | the mail addresses to notify      |
| keepalived_vrrp_instances     | hash  | no       |                       | the vrrp instances                |
| keepalived_vrrp_scripts       | hash  | no       |                       | the vrrp check scripts            |
| keepalived_track_files        | hash  | no       |                       | the track files                   |

### keepalived_vrrp_instances

| Name                | Type  | Required | Default | Comment                                      |
| ------------------- | ----- | -------- | ------- | -------------------------------------------- |
| key                 | str   | yes      |         | the instance name                            |
| state               | str   | yes      |         | Initial state, MASTER or BACKUP              |
| interface           | str   | yes      |         | interface for inside_network, bound by vrrp  |
| virtual_router_id   | int   | yes      |         | arbitrary unique number from 1 to 255        |
| priority            | int   | yes      |         | for electing MASTER, highest priority wins   |
| smtp_alert          | bool  | yes      |         | send SMTP alerts                             |
| authentication      | hash  | no       |         | use an authentication `deprecated`           |
| virtual_ipaddresses | array | yes      |         | addresses add on change to MASTER, to BACKUP |
| track_scripts       | array | no       |         | add a tracking script to the interface       |
| track_files         | array | no       |         | add a tracking file to the interface         |
| notify              | hash  | no       |         | notify scripts, alert as above               |

Example:

```
vip_ngninx:
  state: master
  interface: eth0
  virtual_router_id: 5
  priority: 200
  smtp_alert: true
  authentication:
    type: pass
    pass: 12345678
  virtual_ipaddresses:
    - 172.255.0.254/32 dev eth0
  track_scripts:
    - check_nginx
  notify:
    master: /usr/bin/notification.sh

```

### keepalived_vrrp_scripts

| Name     | Type | Required | Default | Comment                               |
| -------- | ---- | -------- | ------- | ------------------------------------- |
| key      | str  | yes      |         | the script name                       |
| script   | str  | yes      |         | path of the script to execute         |
| interval | int  | no       | 2       | seconds between script invocations    |
| fall     | int  | no       | 2       | number of successes for KO transition |
| rise     | int  | no       | 2       | number of successes for OK transition |
| weight   | int  | no       | 1       | weigth of script                      |

Example:

```
check_nginx:
  script: /usr/local/bin/check_nginx
  fall: 3
  rise: 5
  interval: 10
```

### keepalived_track_file

| Name   | Type | Required | Default | Comment                   |
| ------ | ---- | -------- | ------- | ------------------------- |
| key    | str  | yes      |         | the track file name       |
| file   | str  | yes      |         | path of the file to track |
| weight | int  | no       | 1       | weigth of file            |

Example:

```
master_file:
  file: /tmp/master
  weight: 3
```

## How to use

```
- hosts: server
  roles:
    - keepalived
  vars:
    keepalived_vrrp_scripts:
      - name: check_file
        script: test -e /path/file
        interval: 5
    keepalived_vrrp_instances:
      - name: test
        state: master
        interface: eth0
        virtual_router_id: 5
        priority: 200
        virtual_ipaddresses:
          - 172.255.0.254/32 dev eth0
        track_scripts:
          - check_file
        notify:
          master: touch /tmp/keepalived
```

## Development

### Test with molecule and docker

- install [docker](https://docs.docker.com/engine/installation/)
- install `python3` and `python3-pip`
- install molecule and dependencies `pip3 install molecule 'molecule[docker]' docker ansible-lint testinfra yamllint`
- run `molecule test`

## License

```
Copyright (c) 2019 Adrien Waksberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
