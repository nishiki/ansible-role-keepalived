import testinfra.utils.ansible_runner

def test_packages(host):
  package = host.package('keepalived')
  assert package.is_installed

def test_config_file(host):
  path = host.file('/etc/keepalived/keepalived.conf')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'root'
  assert path.mode == 0o600
  assert path.contains('priority 200')
  assert path.contains('interval 10')
  assert path.contains('track_file master {')

def test_service(host):
  service = host.service('keepalived')
  assert service.is_running
  assert service.is_enabled
