# CHANGELOG

This project adheres to [Semantic Versioning](http://semver.org/).
Which is based on [Keep A Changelog](http://keepachangelog.com/)

## [Unreleased]

### Added

- add track files

### Changed

- keepalived_vrrp_scripts variable is now a hash
- keepalived_vrrp_instances variable is now a hash
- test: test: use personal docker registry

### Fixed

- missing bracket for vrrp_sript in template

## v1.1.0 - 2021-08-18

### Added

- support debian 11
- support debian 10 and ansible 2.9

### Changed

- test: replace kitchen to molecule
- chore: use FQCN for module name

### Removed

- support debian 9

## v1.0.0 - 2019-04-12

- first version
